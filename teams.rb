class Team
  def sort
    goalies = File.readlines("./goalies.txt")
    players = File.readlines("./players.txt")

    # Do the sort
    players_per_team = players.size/2
    team = players.shuffle.each_slice(players_per_team).to_a
    goalie = goalies.shuffle.each_slice(1).to_a
    # Add the players
    red_team = team[0]
    blue_team = team[1]
    # Add goalies
    red = red_team.push(goalie[0])
    blue = blue_team.push(goalie[1])

    puts "Equipo Rojo:"
    puts red
    puts " "
    puts "Equipo Azul:"
    puts blue
  end
end

Team.new.sort
